# About This Repo
This repository hosts, unless simply otherwise impossible on a technical level, the configurations, customizations, DevOps tooling, etc required to take a vanilla OpenEdX distribution and turn it into a Tailored Labs CMS/LMS instance. 

Much like anything in software development, that aforementioned is a theoretical ideal state that we're working towards and many compromises, to-dos, nice-to-haves, etc remain as this is very much a work in progress. 

# Standards, Protocols, Etc
We're still forming this, but for now, please make commits starting or ending with WIP for works in progress. Code style guides, lint standards/linter usage, issue reporting, bugs, etc will be things we should add here as we mature. Declaring releases using tags with semantic versioning is recommended, and as we move to single-tenant deployments, we should consider semver with customer-specific versions to allow for complete customer-specific code modifications.

# General Things To Know

This isn't exhaustive, but designed to get an intelligent developer oriented to things specific to where we are in our phase of progress, OpenEdX, or our processes here. 

* OpenEdX is a mashup of a lot of technologies. It's pretty well architected but it is very complex and clunky.
* Because OpenEdX lacks even a passable good "live reload" system for development, UI customizations can be more onerous than you'd think (and, frankly, should be in 2019)....SOOOOO....Use Chrome DevTools to test changes to CSS, inspect computed values to see where certain values are being set to figure out the right selector to use (I recommend verbatim copying said selector from the Dev Tools and using it to save headache and errors and inserting it into the file recommended below in common task notes), so you can create the most specific CSS selector to override things in the theme, otherwise you maybe frustrated by the 10+ mins it takes to build assets just to see your stuff fail b/c of wonky CSS inheritance. The "COMPUTED" tab in Chrome DevTools is your ally here more than you'd think because of this. 
* You may have to use the !important keyword, especially when overriding CSS in course UI components. This is not optimal, but it is how the Mako/Django templates render things. If you are flummoxed why your CSS isn't working, this maybe why. The theme CSS, theoretically, SHOULD be the last thing included and thus "dominates" but I've found that is not always true in the courses CSS. 


# DevOps/Building/CI/CD

## Overview
OpenEdX is, well, uh, not really the most friendly to CI/CD. In fact, it's downright uncooperative on a good day. 

That being said, we are trying to take it, kicking and screaming, into a place where we have modularized, containerized, automated builds.

For now, any tag will be rsync'ed to the production server, assets will be compiled, and all daemons restarted. So, you can consider that any time you tag anything, it's a production build. SOOOO, be careful with that, especially as we are yet to have a good set of tests (unit, functional, system, etc) in place to prevent bad builds!

We are moving towards having everything hosted on GKE on a Kubernetes cluster where each client is in its own deployment in a its own k8s namespace. This will require some work to get the current Tudor Docker/k8s support built out into a proper Helm chart to support this. In the mean time we have a said k8s cluster deployed on GKE where the builds run, which currently rsync this repo over to the production server, assets are compiled, and server restarted. 

## Current Architecture Overview
This diagram describes (with a variety of details of internal GitLab components/runners on the Kubernetes deployment glossed over as they are handled by GitLab) the current architecture of the CI/CD set up. 
![Current CI/CD Diagram](docs/current-cicd.jpg "Current CI/CD Diagram")


1. A TAG is declared in GitLab and a build pipeline is triggered. This pipeline is called "production" in the GitLab configurations. The file .gitlab-ci.yml in this repo is the configuration that defines the steps of this process and references environmental variables defined in GitLab for extensibility & security purposes (to keep secrets out of VCS and plain text files). Namely:
    * Deploy Host IP/Host (```DEPLOY_HOST```) - The IP or Hostname of the target system to deploy to.  This process was built around an existing process as an incremental improvement without spending too much time modernizing things so this is a "legacy compatibility/migration" step/config item to output build artifacts to an existing server rather than the final fully containerized system. As, such, it is expected that this system be configured exactly as the AWS production server we currently have, including a variety of scripts in /home/ubuntu, etc. 
    * Deploy Host SSH Key (```DEPLOY_SSH_KEY```) - The SSH key for the above system.
2. GitLab triggers the GitLab runner to begin the pipeline on the GKE k8s cluster ```tailored-labs-k8s``` in the ```emotion-recognition-dashboard``` GCloud project. 
3. The commit represented by the tag is rsync'ed to the target machine.
4. The compile assets script is executed on the target machine.
5. The restart stack script is executed on the target machine. 

## v1 Goal State Overview

It is quite similar to the above/current architecture, however, instead of simply pushing repo state via rsync and rebuilding assets on our existing production server, a Helm chart is deployed directly to the same k8s cluster on which the build is occuring. Each client has their own environment represented by a release/tag and that branch of code is built as containers and deployed into its own namespace on k8s, this allows each client to have custom code on their own release schedule (see above commentary about semver usage to track and partition this). Clients access the running instances by subdomain where the nginx-ingress controller properly routes it to the given instance. 

In the current architecture, the AWS machine is removed and the GKE system is expanded to server client requests directly using the deployments it hosts directly to clients as below.

![v1 Goal CI/CD Diagram](docs/v1-goal-cicd.jpg "v1 Goal CI/CD Diagram")


# Common Task Notes / So-Far-Best-Practices

## Making UI/UX Changes To The LMS or CMS

This assumes you've reviewed the "General Things To Know" above.

* The first place to try and do this is in the theme. If you can get away with doing it in the CSS you'll have a more replicable, maintainable, and upgrade-proofed solution. The two first places to check are:
    * ```themes/lms/sass/partials/lms/theme/_variables.scss``` - For things that are global CSS variables expressed in SCSS you can and should try doing it here first, IMO.
    * ```themes/lms/static/tailoredlabs.css``` - For things that aren't easy/fun to do or possible in the above

* The actual template html file (e.g. CSS or JS in the file that overrides/changes things). The above two are more advisable for a variety of reasons, but not always possible...

* You will likely have to compile assets and test it. There are scripts in the /home/ubuntu directory on the production server were you can rebuild assets, etc. But the CI/CD automation will do this all for you, so you don't have to worry about it. 
 
## Restarting Server

* ```/home/ubuntu``` on the server has stack restart scripts

## Viewing Deploy Status

Goto [GitLab Pipelines for the edx.config project](https://gitlab.com/tailored-labs/edx-config/pipelines). You can view status on there, including the output of the Compile LMS/Studio processes.

Note that Compile LMS/Studio can only happen for one deploy at a time. The scripts uses a rudimentary lock file concept to prevent multiple asset compliations at once. 

The practical effect of this is that multiple simulataneous builds won't work. I have modified the compile assets script to WAIT until this lock file disappears, which has the practical effect of "serializing" (in quotes for a reason, see next paragraph) all builds (as GitLab does not yet support setting concurrency of builds to 1...that feature is in the works apparently, and all workarounds are quite hack-job-ish). 

The side effect of this is, of course, if said lock file is not cleanly removed, builds may simply stop because all builds behind the affected process will simply be waiting for this file to disappear (another side effect, which is why "serialized" is in quotes above, is that which build goes next is indeterminate and has more to do with the random sleep() function and OS scheduling at the moment than anything else, this wasn't a big enough deal to warrant a true locking system or queuing system as its a small edge case with little to not actual impact at the moment). In order to get around this, if a build is cancelled before it's completed or if it fails and leaves this lock file in place, SSHing into the production box and removing the ```/home/ubuntu/.compile_assets``` file will allow the next process to check for said file to proceed.

## TO-DO

* Full k8s-ization of deploy
* Proper build serialization
* Basic smoke tests to prevent obviously/painfully bad builds
  * HIGH PRIORITY (easier to do, prevents big fails): Bad CSS, Breaking CSS, Broken Asset Compile fails the pipeline/restart
* Others...

