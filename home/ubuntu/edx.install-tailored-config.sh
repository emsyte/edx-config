#!/bin/sh

echo 1. pull latest changes from config repo
cd ~/edx-config
git pull
cd ~

echo 2. copy latest changes to open edx configuration
sudo cp /home/ubuntu/edx-config/edx/app/edxapp/*.json /edx/app/edxapp/
sudo chown edxapp /edx/app/edxapp/*.json
sudo chgrp www-data /edx/app/edxapp/*.json

echo 3. install theme
sudo rm -rf /edx/app/edxapp/edx-platform/themes/tailoredlabs
sudo cp -R /home/ubuntu/edx-config/themes/tailoredlabs /edx/app/edxapp/edx-platform/themes/


# Copy our modified nginx configurations for lms and cms
# these contain lets encrypt ssl certificate and https redirection
#cp /root/edx-config.roverbyopenstax.com/edx/app/nginx/sites-available/lms /edx/app/nginx/sites-available/
#cp /root/edx-config.roverbyopenstax.com/edx/app/nginx/sites-available/cms /edx/app/nginx/sites-available/

# copy server-vars and any other mods to ansible-related work flows
#cp /root/edx-config.roverbyopenstax.com/edx/app/edx_ansible/*.* /edx/app/edx_ansible/

#sudo service nginx restart
